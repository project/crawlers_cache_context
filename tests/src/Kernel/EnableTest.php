<?php

namespace Drupal\Tests\crawlers_cache_context\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Tests the most basic functionality of the module.
 *
 * @group crawlers_cache_context
 */
class EnableTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'crawlers_cache_context',
  ];

  /**
   * Test enable.
   */
  public function testEnable() {
    self::assertTrue(TRUE);
  }

}
