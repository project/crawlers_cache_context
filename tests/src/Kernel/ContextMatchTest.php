<?php

namespace Drupal\Tests\crawlers_cache_context\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Tests the most basic functionality of the module.
 *
 * @group crawlers_cache_context
 */
class ContextMatchTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'crawlers_cache_context',
  ];

  /**
   * Test the matching.
   *
   * @dataProvider providerTestMatch
   */
  public function testMatch($value, $expected, $parameter = NULL) {
    $matcher = $this->container->get('cache_context.crawlers_cache_context');
    $this->container->get('request_stack')->getCurrentRequest()->headers->set('user-agent', $value);
    $value = $matcher->getContext($parameter);
    self::assertEquals($expected, $value);
  }

  /**
   * Data provider for the test.
   */
  public static function providerTestMatch() {
    return [
      [
        'totally a human person with its browser over here. like totally',
        'not-crawler',
      ],
      ['googlebot', 'crawler'],
      ['googlebot', 'not-crawler', 'bingbot'],
      ['googlebot', 'crawler.googlebot', 'googlebot'],
    ];
  }

}
