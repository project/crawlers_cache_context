<?php

namespace Drupal\crawlers_cache_context;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\Context\CacheContextInterface;
use Drupal\Core\Cache\Context\CalculatedCacheContextInterface;
use Jaybizzle\CrawlerDetect\CrawlerDetect;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Cache Context Service For Crawlers.
 */
class CrawlersCacheContext implements CalculatedCacheContextInterface, CacheContextInterface {

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Crawler detect.
   *
   * @var \Jaybizzle\CrawlerDetect\CrawlerDetect
   */
  protected $crawlerDetect;

  /**
   * Constructs a new RequestStackCacheContextBase class.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Jaybizzle\CrawlerDetect\CrawlerDetect $crawlerDetect
   *   Crawler detect.
   */
  public function __construct(RequestStack $request_stack, CrawlerDetect $crawlerDetect) {
    $this->requestStack = $request_stack;
    $this->crawlerDetect = $crawlerDetect;
  }

  /**
   * {@inheritdoc}
   */
  public function getContext($parameter = NULL) {
    $user_agent = $this->requestStack->getCurrentRequest()->headers->get('user-agent');
    if ($parameter === NULL) {
      if ($this->crawlerDetect->isCrawler($user_agent)) {
        return 'crawler';
      }
    }
    else {
      if ($this->crawlerDetect->isCrawler($user_agent)) {
        // This outputs the rather confusing name of "matches". It will just
        // give the match. So use that, in lower case, to determine if we want
        // to make this a "hit" or not.
        $matches = $this->crawlerDetect->getMatches();
        $lower_name = mb_strtolower($matches);
        if ($matches &&  $lower_name == $parameter) {
          return 'crawler.' . $lower_name;
        }
      }
    }
    return 'not-crawler';
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata($parameter = NULL) {
    return new CacheableMetadata();
  }

  /**
   * {@inheritdoc}
   */
  public static function getLabel() {
    return t('Crawlers cache context');
  }

}
